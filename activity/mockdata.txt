=====Users (ATLEAST 2 - ADMIN AND NON-ADMIN=====
{
    _id: ObjectId,
    firstName: String,
    lastName: String,
    email: String,
    password: String
    isAdmin: Boolean,
    mobileNo: String,
    orders:[
    {
        productId: ObjectId,
        productName: String,
        price: Number
        transactionDate: Date
    }
]
}


===== Order =====
{
    _id: ObjectId, //userId
    _id: ObjectId, // orderId
    transactionDate: Date
    status: String,
    Total: Number
}

===== Products (ATLEAST 2 - ACTIVE AND INACTIVE PRODUCTS) =====
  {
    _id: ObjectId,
    name: String,
    description: String,
    price: Number,
    stocks: Number,
    isActive: Boolean,
    SKU: String
}

===== Order Products =====
 {
    orderId: ObjectId,
    productId: ObjectId,
    quantity: Number,
    price: Number,
    subTotal: Number

 }   