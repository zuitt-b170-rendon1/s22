Course Booking App Data Model.txt

=====USERS=====
{
	_id: ObjectId,
	firstName: String,
	lastName: String,
	email: String,
	password: String
	isAdmin: Boolean
	mobileNo: String,
	enrollments:[
	{
		courseId: ObjectId,
		courseName: String,
		enrolledOn: Date
	}
	]
}


=====Courses=====
{
	_id: ObjectId,
	name: String,
	description: String,
	price: Number,
	isActive? Boolean,
	slots: Number,
	enrollees: [
	{
		userId: ObjectId,
		enrolledOn: Date,
	}
 ]
 	createdOn: Date
}



=====Transactions=====

{
	_id: ObjectId,
	userId: ObjectId,
	courseId: ObjectId,
	totalAmount: Numbers,
	isPaid: Boolean,
	paymentMethod: String,
	dateTimeCreated: Date
}

Data model Design
  the main consideration for the structure of your documents in your database

==Emebedded Data Model==
Company Data Model (branch is embedded)
{
	"_id": ObjectId,
	"name": "ABC Company",
	"contact": "09123456789",
	"branches": [
	{
		"name": "branch1",
		"address": "Quezon City",
		"contact": "09987654321"	
	}
	]
}

==Reference Data Model==
//1 or more fields are being referenced from other data model

Company Data Model
{
	"_id": ObjectId,
	"name": "ABC Company",
	"contact": "09123456789",
}

Brances Data Model
{
	"_id": ObjectId,
	"companyId": ObjectId,//this is the ID coming from the company data mode
	"name": "branch1",
	"address": "Quezon City",
	"contact": "09987654321"	
}

{
	"_id": ObjectId,
	"companyId": ObjectId,//this is the ID coming from the company data mode
	"name": "branch2",
	"address": "Thailand",
	"contact": "09987654321"	
}

